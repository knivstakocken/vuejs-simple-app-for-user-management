const path = require("path");

module.exports = {
  publicPath: '/nodejs-mongodb-auth/',
  transpileDependencies: [/node_modules[/\\\\]vuetify[/\\\\]/],
  productionSourceMap: false,
  lintOnSave: true,

  pwa: {
    themeColor: '#3BABDC',
    msTileColor: '#3BABDC'
  }
}