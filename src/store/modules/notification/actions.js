// import router from '@/router';
import * as user from '@/api/user';

const login = ({ commit }, payload) => {
  console.log('login wtf!!!');

  commit('SET_LOADING_STATE', true);
  user.authenticate(payload.username, payload.password).then(
    (res) => {
      commit('SET_TOKEN', res.data.token);
      commit('SET_LOADING_STATE', false);
    },
    (err) => {
      commit('SET_USER', null);
      commit('SET_LOADING_STATE', false);
    }
  );
};

const register = ({ commit }, payload) => {
  user.register(payload).then(
    (res) => console.log(res),
    (err) => console.log(err)
  );
};

const logout = ({ commit }) => {
  commit('SET_USER', null);
};

const isAuthenticated = (state) => {
  return state.user || false;
};

export default {
  login,
  register,
  logout,
  isAuthenticated
};
