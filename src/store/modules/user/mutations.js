const SET_TOKEN = (state, token) => {
  state.token = token;
};

const SET_LOADING_STATE = (state, loading) => {
  state.loading = loading;
};

export default {
  SET_TOKEN,
  SET_LOADING_STATE
};
