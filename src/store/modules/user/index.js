import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export default {
  namespaced: true,
  state: {
    token: null,
    loading: false
  },
  actions,
  getters,
  mutations
};
