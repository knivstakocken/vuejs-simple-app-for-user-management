const token = (state) => {
  return state.token;
}

export default {
  token
};
