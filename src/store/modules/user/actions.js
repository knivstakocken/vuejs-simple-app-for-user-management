import router from '@/router';
import * as user from '@/api/user';

const login = ({ commit }, payload) => {
  commit('SET_LOADING_STATE', true);

  user.authenticate(payload.username, payload.password).then(
    (res) => {
      commit('SET_TOKEN', res.data.token);
      console.log(res);
      commit('SET_LOADING_STATE', false);

      router.push({ name: 'profile' });
    },
    (err) => {
      // commit('TOKEN', null);
      commit('SET_LOADING_STATE', false);
      console.log(err);
    }
  );
};

const register = ({ commit }, payload) => {
  user.register(payload).then(
    () => {
      router.push({ name: 'login' });
    },
    (err) => {
      console.log(err);
    }
  );
};

const update = ({ commit }, payload) => {
  user.update(payload).then(
    (res) => {
      console.log(res);
    },
    (err) => {
      console.log(err);
    }
  );
};

const current = () => {
  return user.current().then(res => {
    return res.data;
  });
}

const logout = ({ commit }) => {
  commit('SET_TOKEN', null);
  router.push({ name: 'login' });
};

const isAuthenticated = (state) => {
  return state.user || false;
};

export default {
  login,
  logout,
  current,
  update,
  register,
  isAuthenticated
};
