import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import storeUserModule from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  plugins: [createPersistedState({
    paths: [
      'user.token'
    ],
  })],
  modules: {
    user: storeUserModule
  }
});
