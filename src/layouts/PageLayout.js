export default {
  name: 'PageLayout',
  props: {
    name: {
      type: String,
      default: 'Blank'
    }
  },
  created() {
    import(`./Layout${this.name}.vue`).then((res) => {
      this.$parent.$emit('update:layout', res.default);
    });
  },
  render() {
    return this.$slots.default[0];
  }
}