import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/dashboard',
      name: 'dashboard',
      meta: { requiredAuth: true },
      component: () => import(/* webpackChunkName: "routes" */ './views/Dashboard.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "routes" */ './views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "routes" */ './views/Register.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      meta: { requiredAuth: true },
      component: () => import(/* webpackChunkName: "routes" */ './views/Profile.vue')
    },
    {
      path: '',
      redirect: '/login'
    },
    {
      path: '*',
      name: 'notfound',
      component: () => import(/* webpackChunkName: "routes" */ './views/NotFound.vue'),
    }
  ]
})
