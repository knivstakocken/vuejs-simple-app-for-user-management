import { query } from './query';

export const register = async (payload) => {
  return await query('post', '/users/register', payload);
}

export const authenticate = async (username, password) => {
  return await query('post', '/users/authenticate', { 
    username, 
    password 
  });
}

export const update = async (payload) => {
  return await query('put', '/users/update', payload);
}

export const current = async () => {
  return await query('get', '/users/current');
}
