import axios from 'axios';
import store from '@/store';

let BASE_URL = {
  production: 'http://192.168.0.102:4000',
  development: 'http://localhost:4000'
}

const api = axios.create({
  baseURL: BASE_URL[process.env.NODE_ENV],
  withCredentials: false
});

export const query = async (type, ...params) => {
  api.interceptors.request.use(
    (config) => {
      const token = store.getters['user/token'];

      if (token) {
        config.headers['Authorization'] = `Bearer ${ token }`
      }
  
      return config
    },
  
    (error) => {
      return Promise.reject(error)
    }
  );

  try {
    const response = await api[type](...params);
    return response;
  } catch (error) {
    return error.response;
  }
}