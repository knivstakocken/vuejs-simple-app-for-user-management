import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store/'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import PageLayout from './layouts/PageLayout'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.meta.requiredAuth && !store.getters['user/token']) {
    next({ name: 'login' });
  } else {
    next();
  }
});

Vue.component('PageLayout', PageLayout);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
